console.log('1. Range')
function range(startNum, finishNum)
{
if (startNum == null || finishNum == null)
{
return -1;
}
else if (startNum < finishNum)
{
var hasil = []
for (i=startNum; i<=finishNum; i++)
{
hasil.push (i)
}
return hasil;
}
else if (startNum > finishNum)
{
var hasil = []
for (i=startNum; i>finishNum; i--)
{
hasil.push (i)
}
return hasil;
}
}
console.log(range(1, 10))
console.log(range(1))
console.log(range(11,18))
console.log(range(54, 50))
console.log(range())

console.log('2. Range with Step')
function rangeWithStep(startNum, finishNum, step)
{
if (startNum == null || finishNum == null)
{
return -1;
}
else if (startNum < finishNum)
{
var hasil = []
for (i=startNum; i<=finishNum; i+=step)
{
hasil.push (i)
}
return hasil;
}
else if (startNum > finishNum)
{
var hasil = []
for (i=startNum; i>finishNum; i-=step)
{
hasil.push (i)
}
return hasil;
}
}
console.log(rangeWithStep(1, 10, 2)) 
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))

console.log('3. Sum of Range')
function sum(startNum, finishNum, deret)
{
if (deret == null)
{
deret = 1
}
if (startNum == null && finishNum == null)
{
return 0;
}
else if (finishNum == null)
{
return 1;
}
else if (startNum < finishNum)
{
var hasil = 0
for (i=startNum; i<=finishNum; i+=deret)
{
hasil = hasil + i
}
return hasil;
}
else if (startNum > finishNum)
{
var hasil = 0
for (i=startNum; i>=finishNum; i-=deret)
{
hasil = hasil + i
}
return hasil;
}
}
console.log(sum(1,10)) 
console.log(sum(5, 50, 2)) 
console.log(sum(15,10)) 
console.log(sum(20, 10, 2)) 
console.log(sum(1)) 
console.log(sum())

console.log('4. Array Multidimensi')
function dataHandling(input)
{
var n=input.length
for (i=0; i<n; i++)
{
var nn=input[i].length
console.log("Nomor ID : "+input[i][0])
console.log("Nama Lengkap : "+input[i][1])
console.log("TTL : "+input[i][2])
console.log("Hobi : "+input[i][3])
console.log("")
}
}
var input = [
["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989","Membaca"],
["0002", "Dika Sembiring", "Medan", "10/10/1992", "BermainGitar"],
["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
["0004", "Bintang Senjaya", "Martapura", "6/4/1970","Berkebun"]
]
console.log(dataHandling(input))

console.log('5. Balik Kata')
function balikKata(kata)
{
var katabaru = " "
for (i=(kata.length-1); i>=0; i--)
{
katabaru = katabaru+kata[i];
}
return katabaru;
}
console.log(balikKata("Kasur Rusak")) 
console.log(balikKata("Informatika")) 
console.log(balikKata("Haji Ijah")) 
console.log(balikKata("racecar")) 
console.log(balikKata("I am Humanikers" ))

console.log('6. Metode Array')
var input = ["0001", "Roman Alamsyah Elsharawy", "Bandar Lampung", "21/5/1989", "membaca"];
var data1 = input[0]
var data2 = input[1]
var data3 = input[2]
var data4 = input[3]
var data5 = ('SMA Internasional Metro')
var alamat = data3.split(' ')
alamat.unshift('Provinsi')
var newalamat = alamat.join(" ")
var output = (data1+","+data2+","+newalamat+","+data4+",Pria,"+data5)
var newoutput = output.split(",")
console.log(newoutput)
var tanggal = data4.split('/')
var newtanggal = tanggal.join("-")
var bulan = tanggal[1] = 5
switch(bulan){
	case 1: {console.log('Januari'); break;}
	case 2: {console.log('Febuari'); break;}
	case 3: {console.log('Maret'); break;}
	case 4: {console.log('April'); break;}
	case 5: {console.log('Mei'); break;}
	case 6: {console.log('Juni'); break;}
	case 7: {console.log('Juli'); break;}
	case 8: {console.log('Agustus'); break;}
	case 9: {console.log('September'); break;}
	case 10: {console.log('Oktober'); break;}
	case 11: {console.log('November'); break;}
	case 12: {console.log('Desember'); break;}
default: {console.log('Tanggal tidak terdeteksi'); break;}}
var numbers = tanggal
numbers.sort()
var nama = data2.split(' ')
nama.pop()
var newnama = nama.join(" ")
console.log(numbers)
console.log(newtanggal)
console.log(newnama)