1. //Function
console.log('1. Function')
   function teriak()
   {
    return "Halo Humanika"
   }

//Cara memanggil
console.log(teriak())

console.log('2. Kalikan')
function kalikan(num1, num2)
{
return num1 * num2;
}
var num1 = 12;
var num2 = 4;
var hasilkali = kalikan(num1, num2);
console.log(hasilkali)

console.log('3. Introduce')
function introduce(name, age, address, hobby)
{
return '"Nama saya '+name+', umur saya '+age+'tahun , alamat saya di '+address+', dan saya punya hobby yaitu '+hobby+'!"';
}
var name = "Ira Dwi Nugrayani";
var age = 20;
var address = "Perum Citalang Indah, Blok J.15 Rt/Rw 22/005";
var hobby = "Drakoran";
var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan)
//Menampilkan "Nama saya Ira Dwi Nugrayani, Umur saya 20, Alamat saya Perum Citalang Indah, Blok J.15 Rt/Rw 22/005, dan Hobby saya Drakoran"